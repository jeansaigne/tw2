<?php
	abstract class Element
	{
		protected $attributes = array();
		
		/**
		 * Create an Element with no default attributes
		 * @param attributes : The associative array with HTML attributes
		 */
		function __construct($attributes = array())
		{
			$this->attributes = $attributes;
		}
		
		/* ----- Getters ----- */
		/**
		 * Get a string with all HTML attributes for this element
		 * @return the attributes string
		 */
		public function getAttributes()
		{
			$str = ' ';
			foreach ($this->attributes as $key => $value)
			{
				$str .= $key . '="' . $value . '" ';
			}
			return preg_replace('#(.+) #','$1', $str);
		}
		
		/* ----- Setters ----- */
		/**
		 * Add an attributes and a value to this attributes for this element, doesn't
		 * replace existing value, juste concatenate with.
		 */
		public function addAttribute($attributeType, $attributeValue)
		{
			if (isset($this->attributes[$attributeType]))
			{
				$this->attributes[$attributeType] = $this->attributes[$attributeType] . ' ' . $attributeValue;
			}
			else
			{
				$this->attributes[$attributeType] = $attributeValue;
			}
		}
		
		/* ----- Printer ----- */
		/**
		 * Get the HTML representation of this element
		 * @return The HTML representation of this element
		 */
		abstract public function toHTML();
	}
?>