<?php
	class LinkContent extends Element
	{
		private $url;
		private $textContent;
		private $canBeBlight;
		
		/**
		 * Create a Link with no default attributes
		 * @param url : The url of the link
		 * @param textContent : The associated text
		 * @param canBeBlight : For allow mouse hover effect
		 * @param attributes : The associative array with HTML attributes
		 */
		function __construct($url, $textContent, $canBeBlight = false, $attributes = array())
		{
			parent::__construct($attributes);
			$this->url = $url;
			$this->textContent = $textContent;
			$this->canBeBlight = $canBeBlight;
		}
		
		/* ----- Printers ----- */
		/**
		 * {@inheritDocs}
		 */
		public function toHTML()
		{
			if ($this->canBeBlight && in_array($this->url, explode('/', $_SERVER['PHP_SELF'])))
			{
				$this->addAttribute('class', 'active');
			}
			
			return '
			<a href="' . $this->url . '" ' . $this->getAttributes() . '>
				' . $this->textContent . '
			</a>';
		}
	}
?>