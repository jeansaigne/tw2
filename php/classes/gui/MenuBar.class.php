<?php
	class MenuBar extends Element
	{
		private $menuItems;
		
		/**
		 * Create a MenuBar with no default attributes
		 * @param attributes : The associative array with HTML attributes
		 */
		function __construct($attributes = array())
		{
			parent::__construct($attributes);
			$this->menuItems = array();
		}
		
		/* ----- Setters ----- */
		/**
		 * Add an item to the menu bar
		 * @param element : The element to add into the menu bar
		 */
		public function addMenuItem(Element $e)
		{
			$this->menuItems[] = $e;
		}
		
		/* ----- Printers ----- */
		/**
		 * {@inheritDocs}
		 */
		public function toHTML()
		{
			$elements = '';
			for ($i = 0; $i < sizeof($this->menuItems); $i++)
			{
				$elements .= $this->menuItems[$i]->toHTML();
			}
			
			$profilContent = '';
			if (isset($_SESSION['member']))
			{
				$profilContent .= '<li class="right resize">
					Connecté en tant que ' . $_SESSION['member']->getPseudo() . '
				</li>';
			}
			
			return '
			<nav ' . $this->getAttributes() . '>
				<ul>
					' . preg_replace('#(.+) #', '$1', $elements) . $profilContent . '
				</ul>
			</nav>';
		}
	}
?>