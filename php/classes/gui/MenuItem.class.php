<?php
	class MenuItem extends Element
	{
		private $content;
		
		/**
		 * Create a MenuItem with default attributes class is menuItem
		 * @param content : The content of the MenuItem
		 * @param attributes : The associative array with HTML attributes
		 */
		function __construct(Element $content, $attributes = array())
		{
			parent::__construct($attributes);
			$this->content = $content;
			$this->addAttribute('class', 'menuItem');
		}
		
		/* ---- Printers ---- */
		/**
		 * {@inheritDocs}
		 */
		public function toHTML()
		{
			return '
			<li ' . $this->getAttributes() . '>
				' . $this->content->toHTML() . '
			</li>';
		}
	}
?>