<?php
	class LoginForm
	{
		private $isConnected;
		private $content;
		
		/**
		 * Create the login bar
		 */
		function __construct()
		{
			$this->isConnected = isset($_SESSION['member']);
			
			if ($this->isConnected)
			{
				$this->content = '<div id="loginBar">
					<div class="pageWidth">
						<div class="pageContent">
							<h3 id="loginBarHandle">
								<label for="loginControl">
									<a class="concealed noOutline" href="logout.php">Déconnexion</a>
								</label>
							</h3>
						</div>
					</div>
				</div>';
			}
			else
			{
				$this->content = '<div id="loginBar">
					<div class="pageWidth">
						<div class="pageContent">
							<h3 id="loginBarHandle">
								<label for="loginControl">
									<span class="concealed noOutline">
										S\'identifier ou s\'inscrire
									</span>
								</label>
							</h3>
							<span class="helper"></span>
							<div class="_swOuter">
								<div class="_swInner">
									<form action="" method="POST" id="login" class="loginForm">
										<div class="wrapper">
											<dl class="ctrlUnit">
												<dt>
													<label for="loginControl">
														Votre pseudo:
													</label>
												</dt>
												<dd>
													<input id="loginControl" class="textCtrl" type="text" autofocus="autofocus" name="login" />
												</dd>
											</dl>
											<dl class="ctrlUnit">
												<dt>
													<label for="ctrl_password">
														Avez-vous déjà un compte ?
													</label>
												</dt>
												<dd>
													<ul>
														<li>
															<label for="ctrl_not_registered">
																<input id="ctrl_not_registered" type="radio" name="register" value="1" />
																Non, créez-en un maintenant.
															</label>
														</li>
														<li>
															<label for="ctrl_registered">
																<input id="ctrl_registered" class="Disabler" type="radio" name="register" value="0" checked="checked" />
																Oui, mon mot de passe est:
															</label>
														</li>
														<li id="ctrl_register_Disabler">
															<input id="ctrl_password" class="textCtrl" type="password" name="password" />
														</li>
													</ul>
												</dd>
											</dl>
											<dl class="ctrlUnit">
												<dt></dt>
												<dd>
													<input type="submit" class="button primary" data-loginphrase="Connexion" data-signupphrase="S\'inscrire" value="Connexion" />
												</dd>
											</dl>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}
		}
		
		/* ----- Printers ----- */
		/**
		 * Display the formulary
		 */
		public function toHTML()
		{
			return $this->content;
		}
	}
?>