<?php
	class Security
	{
		/**
		 * Log an user to the forum
		 * @param bdd : The database connexion
		 * @param $login : The username of the user
		 * @param password : The password of the user
		 */
		public static function login($bdd, $login, $password)
		{
			$req = $bdd->prepare('SELECT * FROM members WHERE pseudo = :pseudo AND pass = :pass');
			$req->bindParam('pseudo', $login);
			$req->bindParam('pass', $password);
			$req->execute();
			
			$data = $req->fetch();
			
			return ($data !== false ? Member::fromPseudo($bdd, $login) : null);
		}
		
		/**
		 * Check if an user can connect to the forum with finded informations
		 * @param bdd : The database connexion
		 */
		public static function controlAuthentification($bdd)
		{
			if (isset($_SESSION['member']))
			{
				return true;
			}
			else if (isset($_REQUEST['login']) && isset($_REQUEST['password']) && Security::login($bdd, $_REQUEST['login'], sha1($_REQUEST['password']) . '==') !== null)
			{
				$pass = sha1($_REQUEST['password']) . '==';
				$member = Security::login($bdd, $_REQUEST['login'], $pass);
				$member->setIp($bdd);
				
				$_SESSION['member'] = $member;
				return true;
			}
			return false;
		}
		
		/**
		 * Redirect the user if is not logged
		 * @param bdd : The database connexion
		 * @param linkRedirect : The link to the redirected page
		 */
		public static function redirectIfNotLogged($bdd, $linkRedirect)
		{
			if (!Security::controlAuthentification($bdd))
			{
				header("Location: " . $linkRedirect);
			}
		}
		
		/**
		 * Check if an user is logged
		 * @return true if an user is log, false else
		 */
		public static function isLogged()
		{
			return isset($_SESSION['member']);
		}
	}
?>