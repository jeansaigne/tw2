<?php
	class CustomDateTime
	{
		private $customDateTime;
		
		/**
		 * Create a DateTime
		 * @param customDateTime : The date format
		 */
		function __construct($customDateTime)
		{
			$this->customDateTime = $customDateTime;
		}
		
		/**
		 * Stringify the date time
		 * @return A lisible datetime
		 */
		public function toString()
		{
			$monthsList = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
			
			if (preg_match('#[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}#', $this->customDateTime))
			{
				$year = (int) preg_replace('#([0-9]{4})-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}#', '$1', $this->customDateTime);
				$month = (int) preg_replace('#[0-9]{4}-([0-9]{2})-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}#', '$1', $this->customDateTime);
				$day = (int) preg_replace('#[0-9]{4}-[0-9]{2}-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}#', '$1', $this->customDateTime);
				
				$hour = preg_replace('#[0-9]{4}-[0-9]{2}-[0-9]{2} ([0-9]{2}):([0-9]{2}):[0-9]{2}#', '$1h$2', $this->customDateTime);
				
				$nowYear = (int) date('Y');
				$nowMonth = (int) date('m');
				$nowDay = (int) date('d');
				
				$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));
				$theTime = preg_replace('#([0-9]{4}-[0-9]{2}-[0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}#', '$1', $this->customDateTime);
				
				if ($nowYear == $year && $nowMonth == $month && $nowDay == $day)
				{
					return 'Aujourd\'hui à ' . $hour;
				}
				if ($yesterday == $theTime)
				{
					return 'Hier à ' . $hour;
				}
				return $day . ' ' . $monthsList[$month - 1] . ' ' . $year . ' à ' . $hour;
			}
			else
			{
				$year = (int) preg_replace('#([0-9]{4})-[0-9]{2}-[0-9]{2}#', '$1', $this->customDateTime);
				$month = (int) preg_replace('#[0-9]{4}-([0-9]{2})-[0-9]{2}#', '$1', $this->customDateTime);
				$day = (int) preg_replace('#[0-9]{4}-[0-9]{2}-([0-9]{2})#', '$1', $this->customDateTime);
				
				$nowYear = (int) date('Y');
				$nowMonth = (int) date('m');
				$nowDay = (int) date('d');
				
				$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));
				$theTime = preg_replace('#([0-9]{4}-[0-9]{2}-[0-9]{2})#', '$1', $this->customDateTime);
				
				if ($nowYear == $year && $nowMonth == $month && $nowDay == $day)
				{
					return 'Aujourd\'hui';
				}
				if ($yesterday == $theTime)
				{
					return 'Hier';
				}
				return $day . ' ' . $monthsList[$month - 1] . ' ' . $year;
			}
		}
	}
?>