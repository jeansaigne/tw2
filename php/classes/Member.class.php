<?php
	class Member
	{
		private $pseudo;
		private $ip;
		
		/**
		 * Create a Member
		 * @param bdd: The database connexion
		 * @param pseudo : The member pseudo
		 * @param ip : The member ip
		 */
		function __construct($pseudo, $ip)
		{
			$this->pseudo = $pseudo;
			$this->ip = ($ip == '::1' ? '127.0.0.1' : $ip);
		}
		
		/* ----- Statics ----- */
		/**
		 * Create a member by his pseudo in the forum
		 * @param bdd : The database connexion
		 * @param pseudo : The pseudo of the member
		 * @return a Member object
		 */
		public static function fromPseudo($bdd, $pseudo)
		{
			$req = $bdd->prepare('SELECT * FROM members WHERE pseudo = :pseudo');
			$req->bindParam('pseudo', $pseudo);
			$req->execute();
			
			$data = $req->fetch();
			
			return new Member($data['pseudo'], $data['ip']);
		}
		
		/**
		 * Test if the given pseudo exists in database
		 * @return true if exists, false else
		 */
		public static function checkPseudo($bdd, $pseudo)
		{
			$req = $bdd->prepare('SELECT * FROM members WHERE pseudo = :pseudo');
			$req->bindParam('pseudo', $pseudo);
			$req->execute();
			
			$data = $req->fetch();
			
			return $data !== false;
		}
		
		/* ----- Getters ----- */
		/**
		 * Get the user pseudo
		 * @return the user pseudo
		 */
		public function getPseudo()
		{
			return $this->pseudo;
		}
		
		/**
		 * Get the user ip
		 * @return the user ip
		 */
		public function getIp()
		{
			return $this->ip;
		}
		
		/**
		 * Get the number of posts for this member
		 * @return the number of posts
		 */
		public function getNbEventsPosted($bdd)
		{
			$req = $bdd->prepare('SELECT COUNT(*) FROM events WHERE owner = :pseudo');
			$req->bindParam('pseudo', $this->pseudo);
			$req->execute();
			
			$data = $req->fetch();
			
			return $data[0];
		}
		
		/* ----- Setters ----- */
		/**
		 * Update the user IP
		 * @param bdd : the database connection
		 */
		public function setIp($bdd)
		{
			$ip = ($_SERVER['REMOTE_ADDR'] == '::1' ? '127.0.0.1' : $_SERVER['REMOTE_ADDR']);
			$req = $bdd->prepare('UPDATE members SET ip = :ip WHERE author = :pseudo');
			$req->bindParam('ip', $ip);
			$req->bindParam('pseudo', $this->pseudo);
			$req->execute();
		}
		
		/* ----- SQL ----- */
		/**
		 * Insert the member to the database (registration)
		 */
		public function sqlInsert($bdd, $password)
		{
			$req = $bdd->query('SELECT id FROM members ORDER BY id DESC');
			$data = $req->fetch();
			
			$id = ($data == false ? 1 : $data['id'] + 1);
			
			$req = $bdd->prepare('INSERT INTO members(id, pseudo, ip, pass) VALUES(:id, :pseudo, :ip, :pass)');
			$req->bindParam('id', $id);
			$req->bindParam('pseudo', $this->pseudo);
			$req->bindParam('ip', $this->ip);
			$req->bindParam('pass', $password);
			$req->execute();
			
			var_dump($id, $this);
		}
		
		/**
		 * Unpdate the member from the database (profil save)
		 */
		public function sqlSave($bdd)
		{
			$req = $bdd->prepare('UPDATE members SET ip = :ip WHERE pseudo = :pseudo');
			$req->bindParam('ip', $this->ip);
			$req->bindParam('pseudo', $this->pseudo);
			$req->execute();
		}
		
		/* ----- Printers ----- */
		/**
		 * Get the format for member page
		 * @return the format
		 */
		public function toMemberList($bdd)
		{
			return '
			<li>
				<div class="member">
					' . $this->pseudo . '
					<div class="nbEventsPosted">
						' . $this->getNbEventsPosted($bdd) . '
					</div>
				</div>
			</li>';
		}
	}
?>