<?php
	include('php/DB_connect.php');
	
	header('Content-Type: application/json;charset=UTF-8');
	
	$result = '{"id": 0}';
	
	if (isset($_GET['id']))
	{
		$req = $bdd->prepare('SELECT * FROM events WHERE id = :id');
		$req->bindParam('id', $_GET['id']);
		$req->execute();
		
		$data = $req->fetch();
		
		if ($data !== false)
		{
			$result = '{"id": ' . $data['id'] . ',"lat": ' . $data['lat'] . ',"lon": ' . $data['lon'] . ', "date": "' . $data['postDate'] . '","author": "' . $data['author'] . '","title": "' . $data['title'] . '","text": "' . $data['text'] . '"}';
		}
	}
	
	echo $result;
?>