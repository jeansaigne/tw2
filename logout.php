<?php
	include('php/DB_connect.php');
	
	//Security
	require('php/classes/Member.class.php');
	session_start();
	
	if (isset($_SESSION['member']))
	{
		$member = $_SESSION['member'];
	}
	session_destroy();
	
	header('Location: index.php');
?>