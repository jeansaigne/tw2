function checkUserName()
{
	var userNameField = document.getElementById('ctrlUsername');
	var submit = document.getElementById('submitButton');
	
	var regex = new RegExp(/[a-zA-Z0-9_]+(( ?([a-zA-Z0-9_]+)?)+)?/);
	
	if (regex.test(userNameField.value) && userNameField.value.length >= 4)
	{
		submit.removeAttribute('disabled');
		submit.className = 'button';
	}
	else
	{
		submit.setAttribute('disabled', 'disabled');
		submit.className = 'button disabled';
	}
}

function checkPasswords()
{
	var passwordField = document.getElementById('ctrlPasswordForm');
	var passwordFieldConfirm = document.getElementById('ctrlConfirmPassword');
	var checkValidity = document.getElementById('checkPasswords');
	var submit = document.getElementById('submitButton');
	
	var regex = new RegExp(/.{7}.+/);
	
	if (passwordField.value == passwordFieldConfirm.value && regex.test(passwordField.value))
	{
		checkValidity.innerHTML = '<img src="images/ok.png" alt="Valide" title="Les deux mots de passe sont identiques." />';
		submit.removeAttribute('disabled');
		submit.className = 'button';
	}
	else
	{
		checkValidity.innerHTML = '<img src="images/nope.png" alt="Invalide" title="Les deux mots de passe sont différents."  />';
		submit.setAttribute('disabled', 'disabled');
		submit.className = 'button disabled';
	}
}

function setupEvent()
{
	checkUserName();
	checkPasswords();
	document.getElementById('ctrlUsername').addEventListener('keyup', checkUserName);
	document.getElementById('ctrlPasswordForm').addEventListener('keyup', checkPasswords);
	document.getElementById('ctrlConfirmPassword').addEventListener('keyup', checkPasswords);
}

window.addEventListener('load',setupEvent);