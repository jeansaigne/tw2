<?php
	include('../../../php/DB_connect.php');
	
	require('../../../php/classes/Member.class.php');
	session_start();
	header('charset=UTF-8');
	
	$errlog = array();
	$location = 'Location: ../register.php';
	
	function checkUsername($bdd)
	{
		$username = $_POST['usernameForm'];
		$pattern = '#[a-zA-Z0-9_]+(( ?([a-zA-Z0-9_]+)?)+)?#';
		
		if (isset($username))
		{
			$req = $bdd->prepare('SELECT pseudo FROM members WHERE pseudo = :pseudo');
			$req->bindParam('pseudo', $username);
			$req->execute();
			
			$data = $req->fetch();
			
			if (!$data)
			{
				if (preg_match($pattern, $username))
				{
					if (strlen($username) >= 4)
					{
						return array(0 => true);
					}
					else
					{
						return array(0 => false, 1 => '<span class="red">Votre pseudo doit contenir au moins 4 caractères !</span>');
					}
				}
				else
				{
					return array(0 => false, 1 => '<span class="red">Ce pseudo contient des caractères interdit !</span>');
				}
			}
			else
			{
				return array(0 => false, 1 => '<span class="red">Ce pseudo est déjà utilisé par un autre membre !</span>');
			}
		}
		else
		{
			return array(0 => false, 1 => '<span class="red">Vous devez saisir un pseudo pour vous inscrire !</span>');
		}
	}
	
	function checkPasswords()
	{
		$pass1 = $_POST['passwordForm'];
		$pass2 = $_POST['confirmPassword'];
		$pattern = '#.{7}.+#';
		
		if (isset($pass1))
		{
			if ($pass1 == $pass2)
			{
				if (preg_match($pattern, $pass1))
				{
					return array(0 => true);
				}
				else
				{
					return array(0 => false, 1 => '<span class="red">Votre mot de passe doit contenir au minimum 8 caractères !</span>');
				}
			}
			else
			{
				return array(0 => false, 1 => '<span class="red">Les mots de passe fournis ne sont pas identiques !</span>');
			}
		}
		else
		{
			return array(0 => false, 1 => '<span class="red">Vous devez saisir un mot de passe pour vous inscrire !</span>');
		}
	}
	
	if (checkUsername($bdd)[0] && checkPasswords()[0])
	{
		$username = $_POST['usernameForm'];
		$pass = sha1($_POST['passwordForm']) . '==';
		$ip = ($_SERVER['REMOTE_ADDR'] == '::1' ? '127.0.0.1' : $_SERVER['REMOTE_ADDR']);
		
		$member = new Member($username, $ip);
		$member->sqlInsert($bdd, $pass);
		
		$_SESSION['success'] = 'Merci de vous être inscrit(e), vous pouvez désormais vous connecter !';
		
		$location = 'Location: ../../../index.php';
	}
	else
	{
		if (sizeof(checkUsername($bdd)) == 2)
		{
			$errlog[] = checkUsername($bdd)[1];
		}
		if (sizeof(checkPasswords()) == 2)
		{
			$errlog[] = checkPasswords()[1];
		}
	}
	
	$_SESSION['errlog'] = $errlog;
	
	header($location);
?>