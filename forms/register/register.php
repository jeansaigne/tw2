<?php
	include('../../php/DB_connect.php');
	
	require('../../php/classes/CustomDateTime.class.php');
	
	//GUI
	require('../../php/classes/gui/Element.class.php');
	require('../../php/classes/gui/LinkContent.class.php');
	require('../../php/classes/gui/MenuItem.class.php');
	require('../../php/classes/gui/MenuBar.class.php');
	require('../../php/classes/LoginForm.class.php');
	
	//Security
	require('../../php/classes/Member.class.php');
	require('../../php/classes/Security.class.php');
	session_start();
	
	//Error logger
	$errlog = '';
	$log = '';
	
	if (isset($_SESSION['errlog']))
	{
		for ($i = 0; $i < sizeof($_SESSION['errlog']); $i++)
		{
			$errlog .= '<li>
				' . $_SESSION['errlog'][$i] . '
			</li>';
		}
		
		$log = '<div class="errorsContent">
			<h3 class="errorHeading">
				Veuillez corriger les erreurs suivantes:
			</h3>
			<div class="errors">
				<ol>
					' . $errlog . '
				</ol>
			</div>
		</div>';
		
		unset($_SESSION['errlog']);
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Je m'inscrit</title>
		<link href="../../images/global/icon.png" type="image/png" rel="icon" />
		<link href="../../styles/global.css" type="text/css" rel="stylesheet" />
		<link href="styles/register.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript" src="scripts/register.js"></script>
	</head>
	<body>
		<div id="layout">
			<?php
				$menuBar = new MenuBar(array('id' => 'layout-menuBar'));
				$menuBar->addMenuItem(new MenuItem(new LinkContent('../../index.php', 'Accueil', true)));
				if (Security::isLogged())
				{
					$menuBar->addMenuItem(new MenuItem(new LinkContent('../../addEvent.php', 'Ajouter un événement', true)));
					$menuBar->addMenuItem(new MenuItem(new LinkContent('../../about.php', 'Crédits', true)));
				}
				echo $menuBar->toHTML();
			?>
			<div id="panel">
				<div class="titleBar">
					<h1>S'inscrire</h1>
				</div>
				<form id="formRegister" action="php/register.php" method="POST">
					<?php
						echo '<div id="errorLog">' . $log . '</div>';
					?>
					<dl class="ctrlUnit">
						<dt>
							<label for="ctrlUsername">
								Nom d'utilisateur:
							</label>
						</dt>
						<dd>
							<?php
							$value = (isset($_POST['login']) ? ' value="' . $_POST['login'] . '"' : '');
								echo '<input id="ctrlUsername" class="ctrlText" pattern="[a-zA-Z0-9_]+(( ?([a-zA-Z0-9_]+)?)+)?" required="required" name="usernameForm" autofocus="autofocus" autocomplete="off" maxlength="32" type="text"' . $value . ' />';
							?>
							<p class="explain">
								Il s'agit de votre pseudo définitif, il est unique pour chaque membre et il ne sera plus possible d'en changer.
							</p>
						</dd>
					</dl>
					<fieldset>
						<dl class="ctrlUnit">
							<dt>
								<label for="ctrlPassword">
									Mot de passe:
								</label>
							</dt>
							<dd>
								<input id="ctrlPasswordForm" class="ctrlText" required="required" name="passwordForm" autocomplete="off" pattern=".{7}.+" type="password" />
							</dd>
						</dl>
						<dl class="ctrlUnit">
							<dt>
								<label for="ctrlPassword">
									Retappez votre mot de passe:
								</label>
							</dt>
							<dd>
								<input id="ctrlConfirmPassword" class="ctrlText" required="required" name="confirmPassword" autocomplete="off" pattern=".{7}.+" type="password" />
								<span id="checkPasswords"></span>
								<p class="explain">
									Entrez votre mot de passe dans le premier champ et confirmez-le dans le second.
								</p>
							</dd>
						</dl>
					</fieldset>
					<input id="submitButton" class="button disabled" value="S'inscrire" accesskey="s" type="submit" />
					<br />
				</form>
			</div>
		</div>
	</body>
</html>
