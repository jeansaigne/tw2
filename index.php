<?php
	include('php/DB_connect.php');
	
	require('php/classes/CustomDateTime.class.php');
	
	//GUI
	require('php/classes/gui/Element.class.php');
	require('php/classes/gui/LinkContent.class.php');
	require('php/classes/gui/MenuItem.class.php');
	require('php/classes/gui/MenuBar.class.php');
	require('php/classes/LoginForm.class.php');
	
	//Security
	require('php/classes/Member.class.php');
	require('php/classes/Security.class.php');
	session_start();
	Security::controlAuthentification($bdd);
	
	//Error logger
	$success = '';
	$errlog = '';
	$log = '';
	
	if (isset($_SESSION['errlog']))
	{
		for ($i = 0; $i < sizeof($_SESSION['errlog']); $i++)
		{
			$errlog .= '<li>
				' . $_SESSION['errlog'][$i] . '
			</li>';
		}
		
		$log = '<div class="errorsContent">
			<h3 class="errorHeading">
				Veuillez corriger les erreurs suivantes:
			</h3>
			<div class="errors">
				<ol>
					' . $errlog . '
				</ol>
			</div>
		</div>';
		
		unset($_SESSION['errlog']);
	}
	
	if (isset($_SESSION['success']))
	{
		$success = '<div id="panel">
			<div class="successContent">
				<h3 class="successHeading">
					' . $_SESSION['success'] . '
				</h3>
			</div>
		</div>';
		
		unset($_SESSION['success']);
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Accueil</title>
		<link href="images/global/icon.png" type="image/png" rel="icon" />
		<link href="styles/global.css" type="text/css" rel="stylesheet" />
		<link href="styles/loginBar.css" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
		<script type="text/javascript" src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
		<script type="text/javascript" src="scripts/map.js"></script>
		<script type="text/javascript" src="scripts/auth.js"></script>
	</head>
	<body id="body">
		<?php
			$loginForm = new LoginForm();
			echo $loginForm->toHTML();
		?>
		<div id="layout">
			<?php
				$menuBar = new MenuBar(array('id' => 'layout-menuBar'));
				$menuBar->addMenuItem(new MenuItem(new LinkContent('index.php', 'Accueil', true)));
				if (Security::isLogged())
				{
					$menuBar->addMenuItem(new MenuItem(new LinkContent('addEvent.php', 'Ajouter un événement ', true)));
					$menuBar->addMenuItem(new MenuItem(new LinkContent('about.php', 'Crédits', true)));
				}
				echo $menuBar->toHTML();
				echo $success;
			?>
		</div>
		<div id="map"></div>
		<div id="form">
			<h3>
				Rechercher des événements
			</h3>
			<dl class="ctrlUnit">
				<dt>
					Latitude:
				</dt>
				<dd>
					<span>
						<label>Min:</label><input type="number" id="latMin" />
					</span>
					<span>
						<label>Max:</label><input type="number" id="latMax" />
					</span>
				</dd>
			</dl>
			<dl class="ctrlUnit">
				<dt>
					Longitude:
				</dt>
				<dd>
					<span>
						<label>Min:</label><input type="number" id="lonMin" />
					</span>
					<span>
						<label>Max:</label><input type="number" id="lonMax" />
					</span>
				</dd>
			</dl>
			<dl class="ctrlUnit">
				<dd>
					<label>Auteur:</label><input type="text" id="author" class="ctrlText" placeholder="Pseudo de l'auteur" />
				</dd>
			</dl>
			<dl class="ctrlUnit">
				<dt>
					Date:
				</dt>
				<dd>
					<span>
						<label>Min:</label><input type="text" id="dateMin" placeholder="DD/MM/AAAA" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" />
					</span>
					<span>
						<label>Max:</label><input type="text" id="dateMax" placeholder="DD/MM/AAAA" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" />
					</span>
				</dd>
			</dl>
			<dl class="ctrlUnit">
				<dd>
					<label>Début:</label><input type="number" id="from" placeholder="Laissez vide pour ignorer" min="1" class="ctrlText" />
				</dd>
			</dl>
			<dl class="ctrlUnit">
				<dd>
					<label>Limite:</label><input type="number" id="limit" placeholder="Laissez vide pour ignorer" min="1" class="ctrlText" />
				</dd>
			</dl>
		</div>
	</body>
</html>