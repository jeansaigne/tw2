<?php
	include('php/DB_connect.php');
	
	header('Content-Type: application/json;charset=UTF-8');
	
	function buildQuery($latMin, $latMax, $lonMin, $lonMax, $author, $from, $limit)
	{
		$query = 'SELECT id, lat, lon, postDate, author, title FROM events WHERE lat >= :latMin AND lat <= :latMax AND lon >= :lonMin AND lon <= :lonMax';
		$param = array(
			'latMin' => $latMin,
			'latMax' => $latMax,
			'lonMin' => $lonMin,
			'lonMax' => $lonMax
		);
		
		if (isset($author))
		{
			$query .= ' AND author = :author';
			$param['author'] = $author;
		}
		
		$query .= ' AND id >= :id ORDER BY postDate ASC';
		$param['id'] = $from;
		
		if (isset($limit) && $limit > 0)
		{
			$query .= ' LIMIT ' . $limit;
		}
		
		return array('query' => $query, 'param' => $param);
	}
	
	if (isset($_GET['latMin']) && isset($_GET['latMax']) && isset($_GET['lonMin']) && isset($_GET['lonMax']))
	{
		$latMin = $_GET['latMin'];
		$latMax = $_GET['latMax'];
		$lonMin = $_GET['lonMin'];
		$lonMax = $_GET['lonMax'];
		$author = (isset($_GET['author']) ? $_GET['author'] : null);
		$dateMin = (isset($_GET['dateMin']) ? $_GET['dateMin'] : '0000-00-00 00:00:00');
		$dateMax = (isset($_GET['dateMax']) ? $_GET['dateMax'] : '9999-12-31 23:59:59');
		$from = (isset($_GET['from']) ? (int) $_GET['from'] : 1);
		$limit = (isset($_GET['limit']) ? (int) $_GET['limit'] : null);
		
		$results = '';
		
		$tab = buildQuery($latMin, $latMax, $lonMin, $lonMax, $author, $from, $limit);
		$req = $bdd->prepare($tab['query']);
		$req->execute($tab['param']);
		
		while ($data = $req->fetch())
		{
			$results .= '{"id": ' . $data['id'] . ',"lat": ' . $data['lat'] . ',"lon": ' . $data['lon'] . ', "date": "' . $data['postDate'] . '","author": "' . $data['author'] . '","title": "' . $data['title'] . '"},';
		}
		
		//Display the results and delete the last comma
		echo '{"status": "ok","results": [' . preg_replace('#(.+),#', '$1', $results) . ']}';
	}
	else
	{
		//Missing required param display error
		echo '{"status": "error","results": []}';
	}
?>