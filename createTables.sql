CREATE TABLE members (
	id int(11),
	pseudo varchar(32),
	ip varchar(64),
	pass varchar(128),
	PRIMARY KEY(id, pseudo)
);

-- Les 4 utilisateurs par défaut
INSERT INTO members(id, pseudo, ip, pass) VALUES(1, 'anneparater', '::1', '2b913a3bb173292d6cce4f1a9e7c05d518d3e544==');
INSERT INTO members(id, pseudo, ip, pass) VALUES(2, 'agathezeblouse', '::1', '4840f60f5ae8e6baef6d6c505f8b5763ebb5a97a==');
INSERT INTO members(id, pseudo, ip, pass) VALUES(3, 'nordineateur', '::1', '965dddf34c5a2ea6a92b53c919eaf7bc0421dad6==');
INSERT INTO members(id, pseudo, ip, pass) VALUES(4, 'briceglace', '::1', 'b547decb21de16715429e79b2783f8561cd43406==');

--Pas sûr pour le constraint
CREATE TABLE events (
	id serial,
	lat double,
	lon double,
	postDate timestamp,
	author varchar(32),
	title varchar(40),
	text text,
	PRIMARY KEY(id)
	CONSTRAINT textCons CHECK (char_length(text) > 0 AND char_length(text) <= 1500)
);