/* ----- SQL ----- */
Il te faut cr�� la table 'members' et 'events' dans postegreSql pour ce faire il y a plusieurs mani�res:
- En important le fichier 'createTables.sql'
- En faisant un copier/coller du fichier dans l'espace de saisie de requ�te SQL
- En la recr�ant � la main.

Il faut absolument que la table 'members' soit comme d�crit dans le fichier 'createTables.sql' dans le cas contraire,
je ne peut garatir que tout fonctionnera correctement

Il faut imp�rativement que le champ pour stock� les dates das la table 'events' soit nomm� 'postDate' sinon il y aura complit avec la fonction date de postgreSQL

/* ----- Avancement ----- */
[Fait] 		Liste d'�v�nements acc�cible � tous sans authantification
[Fait]		Carte et mini formulaire pour filtr� les events
[Fait]		Affichier les �v�nements sur la zone visible de la carte
[Fait]		Rafraichir liste des �v�ements quand on zoom/bouge la carte
[A faire]	Refraichir la carte quand mini formulaire est modifier
[A faire]	Refraichir liste des �v�nements quand mini formulaire est modifier
[Fait]		Le nombre d'�v�nements obtenu tien sur une page
[Fait]		Association marqueur/�v�nement trouv�
[Fait]		Chaque �v�nement est affich� de fa�on courte : date, titre, nom de l'auteur
[A faire]	Affichage de la description de l'�v�nement sans perturber la page
[A faire]	Faire disparaitre la descriptio d'un �v�nement une fois celle-ci affich�e
[Fait]		Permettre � un utilisateur de se conecter
[Fait]		Permettre � un utilisateur de cr�� un compte
[A faire]	Permettre la cr�ation d'�v�nement via un formulaire
[A faire]	Permettre la cr�ation d'�v�nement via la carte (obtention latitude & longitude)
[Fait]		Web service pour la recherche d'�v�nements
[Fait]		Web service pour l'obtention d'un �v�nement complet
[A faire]	Tester compatibilit� avec serveur webtp de la fac
[Fait]		Base de donn�e avec interface en PHP via PDO
[A faire]	Valid� les pages PHP au validateur HTML (index.php, register.php et la future addEvent.php)
[Fait]		Fiche de style dans des fichiers � part
[Fait]		Script en javascript/AJAX dans des fichiers � part
[Fait]		Utilis� leaflet pour la cartographie
[Fait]		Pas de conflit pour les droits d'auteur

/* ----- Nombre de fichiers ----- */
Fichiers PHP (dont classes)	: 15
Classes PHP					: 8
Scripts JS					: 3
Scripts AJAX				: 1
Feuille CSS					: 3
Images PNG					: 3

D�pendances Web	: 2
-> Fiche de style pour le framemwork de cartographie leaflet
-> Script JS pour le framemwork de cartographie leaflet

/* ----- Notes suppl�mentaires ----- */
[29/04/2016 15h45]Matthieu : "Je n'ai pour le moment fait que le syst�me d'inscription et de connexion ainsi qu'une �bauche de la page d'accueil."
[29/04/2016 20h09]Matthieu : "Une bonne partie � �t� faite, je continurai demain..."