function display()
{
	var swOuter = document.getElementsByClassName('_swOuter')[0];
	var swInner = document.getElementsByClassName('_swInner')[0];
	var swOuterHeight = parseInt(window.getComputedStyle(swOuter).height);
	
	if (swOuterHeight == 0)
	{
		swOuter.style.height = "240px";
		swInner.style.margin = "0px auto auto";
		document.getElementsByClassName('loginForm')[0].style.display = "block";
	}
	else
	{
		swOuter.style.height = "0px";
		swInner.style.margin = "-240px auto auto";
		document.getElementsByClassName('loginForm')[0].style.display = "none";
	}
}

function ctrlNotRegistered()
{
	var loginControl = document.querySelectorAll('dl.ctrlUnit dt>label[for="loginControl"]')[0];
	var inputLoginControl = document.getElementById('loginControl');
	var passwordInput = document.getElementById('ctrl_password');
	var button = document.querySelectorAll('input.button.primary[type="submit"]')[0];
	
	document.getElementById('login').action = 'forms/register/register.php';
	loginControl.innerHTML = 'Saisissez votre pseudo: ';
	inputLoginControl.pattern = '[a-zA-Z0-9_]+(( ?([a-zA-Z0-9_]+)?)+)?';
	passwordInput.value = '';
	passwordInput.disabled = true;
	button.value = 'Inscription';
}

function ctrlRegistered()
{
	var loginControl = document.querySelectorAll('dl.ctrlUnit dt>label[for="loginControl"]')[0];
	var inputLoginControl = document.getElementById('loginControl');
	var passwordInput = document.getElementById('ctrl_password');
	var button = document.querySelectorAll('input.button.primary[type="submit"]')[0];
	
	document.getElementById('login').action = '';
	loginControl.innerHTML = 'Votre pseudo: ';
	inputLoginControl.pattern = '.+';
	passwordInput.disabled = false;
	button.value = 'Connexion';
}

function deletePanel()
{
	var panelHTML = document.getElementById('panel');
	
	if (panelHTML)
	{
		var remove = function(panelHTML)
		{
			panelHTML.parentNode.removeChild(panelHTML);
		};
		
		window.setTimeout(remove, 5000, panelHTML);
	}
}

function setupEvent()
{
	var e = document.querySelectorAll('span.concealed.noOutline');
	if (e.length > 0)
	{
		e[0].addEventListener('click',display);
	}
	var not_reg = document.querySelectorAll('label[for="ctrl_not_registered"]');
	if (not_reg.length > 0)
	{
		not_reg[0].addEventListener('click',ctrlNotRegistered);
	}
	var reg = document.querySelectorAll('label[for="ctrl_registered"]');
	if (reg.length > 0)
	{
		reg[0].addEventListener('click',ctrlRegistered);
	}
	
	deletePanel();
}

window.addEventListener('load',setupEvent);