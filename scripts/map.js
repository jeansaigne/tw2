﻿//Global variable
var map;
var registeredMarkers;
var popupClick = L.popup().setContent('dernier clic');

/**
 * Delete all markers on the map
 */
function deleteAllMarkers()
{
	for (var i = 0; i < registeredMarkers.length; i++)
	{
		map.removeLayer(registeredMarkers[i]);
	}
	
	registeredMarkers = new Array();
}

/**
 * Make an HTML string with a JSON event
 * @param jsonEvent : [Object] The JSON event as an object
 * @return The HTML string
 */
function jsonToHTML(jsonEvent)
{
	var marker = L.marker([jsonEvent['lat'], jsonEvent['lon']]).addTo(map).bindPopup(jsonEvent['title']);
	
	registeredMarkers.push(marker);
	return '<li>' +
		'<span data-id="' + jsonEvent['id'] + '">' +
			'<span class="author">Créé par ' + jsonEvent['author'] + '</span>' +
			jsonEvent['title'] + ' le ' + jsonEvent['date'] +
		'</span>' +
	'</li>';
}

/**
 * Rewrite date with a specific format
 * @param date : [String] The date to rewrite
 * @param format : [String] The replacement format
 */
function processDate(date, format)
{
	return date.replace(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/, format);
}

/**
 * Update or create the events list with the formulary values
 */
function updateEventsList()
{
	//Delete all markers for adding the news
	deleteAllMarkers();
	
	//Get all formulary params
	var latMin = document.getElementById('latMin').value;
	var latMax = document.getElementById('latMax').value;
	var lonMin = document.getElementById('lonMin').value;
	var lonMax = document.getElementById('lonMax').value;
	var author = document.getElementById('author').value;
	var dateMin = document.getElementById('dateMin').value;
	var dateMax = document.getElementById('dateMax').value;
	var from = document.getElementById('from').value;
	
	//Create the web service URL
	var uri = 'search.php?latMin=' + latMin + '&latMax=' + latMax + '&lonMin=' + lonMin + '&lonMax=' + lonMax;
	if (author && author != '')
		uri += '&author=' + author;
	if (dateMin && dateMin != '')
		uri += '&dateMin=' + processDate(dateMin, '$3/$2/$1');
	if (dateMax && dateMax != '')
		uri += '&dateMax=' + processDate(dateMax, '$3/$2/$1');
	if (from && from != '' && from > 0)
		uri += '&from=' + from;
	if (limit && limit != '' && limit > 0)
		uri += '&limit=' + limit;
	
	//Create the object witch job is to get the event list as a JSON string
	var xhr = new XMLHttpRequest();
	
	xhr.open('GET', encodeURI(uri), true);
	
	//When receive data
	xhr.onload = function()
	{
		var object = JSON.parse(this.responseText);
		if (object['status'] == 'ok' && object['results'].length > 0)
		{
			var eventList = document.getElementById('eventsList');
			if (eventList)
				eventList.parentNode.removeChild(eventList);
			
			var pluralize = (object['results'].length > 1 ? 's' : '');
			var list = document.createElement('div');
			list.id = 'eventsList';
			var content = '';
			
			for (var i = 0; i < object['results'].length; i++)
			{
				content += jsonToHTML(object['results'][i]);
			}
			
			list.innerHTML = '<h3>Événement' + pluralize + ' trouvé' + pluralize + '</h3><ul>' + content + '</ul>';
			
			//Add the event list to the page
			document.getElementById('body').appendChild(list);
		}
	};
	xhr.send(null);
}

/**
 * Update the min/max latitude ad mi/max longitude when move or zoon map
 */
function updateBounds() {
	var bounds = map.getBounds();
	document.getElementById('latMin').setAttribute('value', Math.min(bounds.getNorth().toFixed(5), bounds.getSouth().toFixed(5)));
	document.getElementById('latMax').setAttribute('value', Math.max(bounds.getNorth().toFixed(5), bounds.getSouth().toFixed(5)));
	document.getElementById('lonMin').setAttribute('value', Math.min(bounds.getEast().toFixed(5), bounds.getWest().toFixed(5)));
	document.getElementById('lonMax').setAttribute('value', Math.max(bounds.getEast().toFixed(5), bounds.getWest().toFixed(5)));
	updateEventsList();
}

/**
 * Instanciate the global variable map with a Leaflet map and set the view to the France
 */
function initMap()
{
	registeredMarkers = new Array();
	map = L.map('map').setView([49.4179, 2.8261], 13);
	map.on('load', updateBounds);
    map.on('moveend', updateBounds);
	
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		attribution: '©️ <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
}

/**
 * Initialize the map, register all station, render the map and setup the events for it.
 */
function init()
{
	initMap();
	document.getElementById('latMin').setAttribute('value', Math.min(map.getBounds().getNorth().toFixed(5), map.getBounds().getSouth().toFixed(5)));
	document.getElementById('latMax').setAttribute('value', Math.max(map.getBounds().getNorth().toFixed(5), map.getBounds().getSouth().toFixed(5)));
	document.getElementById('lonMin').setAttribute('value', Math.min(map.getBounds().getEast().toFixed(5), map.getBounds().getWest().toFixed(5)));
	document.getElementById('lonMax').setAttribute('value', Math.max(map.getBounds().getEast().toFixed(5), map.getBounds().getWest().toFixed(5)));
	updateEventsList();
}

window.addEventListener('load', init);
